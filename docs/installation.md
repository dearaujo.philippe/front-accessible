## Installation 
*   yarn install

## Development Mode
*   yarn start:dev
*   visit `http://localhost:3000`

## Style Guide
*   To start development Server : `npx styleguidist server`
*   visit `http://localhost:6060`
*   Build command : `npx styleguidist build`

## Tests
- To launch test `yarn test:unit` or `npm run test:unit`

- Update snapshot: `yarn test:unit:u`

## Linter
To launch linter `yarn lint` or `npm run lint`

## Flow
To launch flow type checking `yarn flow` or `npm run flow`
