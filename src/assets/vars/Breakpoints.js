/* @flow */

export const Breakpoints = {
  Desktop: 767,
  LargeDesktop: 1200
};

export const Layouts = {
  ContainerWidth: 1140,
  SpecialContainerWidth: 1180
};

export const DefaultPadding = {
  Mobile: 15,
  Desktop: 30
};
