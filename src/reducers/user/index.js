/* @flow */

import { combineReducers } from 'redux';
import { passwordRecovery } from './passwordRecoveryRecovery';

export default combineReducers({
  passwordRecovery
});
