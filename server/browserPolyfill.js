if (typeof window === 'undefined') {
  global.window = { location: {} };
}

if (typeof document === 'undefined') {
  global.document = { location: {} };
}
